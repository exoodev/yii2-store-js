<?php

namespace exoo\storejs;

/**
 * StoreJs asset bundle.
 */
class StoreJsAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@npm/store';
    /**
     * @inheritdoc
     */
    public $js = [
        'dist/store.legacy.min.js',
    ];
}
